package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestBabyCounting {
    private val names = arrayOf("a", "A", "b", "B", "c","d","e","f","g","G","h","i","j","J","k","l","m","n",
        "N","o","p","q","r","s", "S","t","u","v","w","x","y", "Y","z","a","b","a","b", "B","a", "A","b","a","b","a","b",
        "y", "y", "y", "y")

    // Kata author's solution
    private fun _babyCount(x: String): Int? {
        val lower = x.lowercase().replace(Regex("[^aby]"), "");
        val b = Regex("b").findAll(lower).toList().size;
        val a = Regex("a").findAll(lower).toList().size;
        val y = Regex("y").findAll(lower).toList().size;
        val count = Math.min(b/2, Math.min(a, y))
        return if (count > 0) count else null
    }

    private fun rand(from: Int, to: Int) : Int {
        return Math.floor((to - from + 1) * Math.random()+from).toInt()
    }

    @Test
    fun testFixed() {
        assertEquals(1, BabyCounting.babyCount("baby"))
        assertEquals(1, BabyCounting.babyCount("abby"))
        assertEquals(3, BabyCounting.babyCount("baby baby baby"))
        assertEquals(1, BabyCounting.babyCount("Why the hell are there so many babies?!"))
        assertEquals(1, BabyCounting.babyCount("Anyone remember life before babies?"))
        assertEquals(2, BabyCounting.babyCount("Happy babies boom ba by?"))
        assertEquals(1, BabyCounting.babyCount("b a b y"))
        assertEquals(null, BabyCounting.babyCount(""))
        assertEquals(null, BabyCounting.babyCount("none her"))
    }

    @Test
    fun testRandom() {
        for (i in 1..100) {
            val str = Array(rand(0, 31)){Array(rand(4, 8)){names[rand(0, names.size - 1)]}.joinToString("")}.joinToString(" ")
            assertEquals(_babyCount(str), BabyCounting.babyCount(str))
        }
    }
}