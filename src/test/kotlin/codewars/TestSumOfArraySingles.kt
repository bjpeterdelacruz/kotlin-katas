package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.random.Random

class TestSumOfArraySingles {
    // Kata author's solution
    private fun sumOfSingleElements(arr: IntArray) =
        arr.filter { v -> arr.count { it == v } == 1 }.sum()

    private fun rand7(): IntArray {
        var len = random(3, 10)
        val switcher = random(0, 10)
        val a: Int
        val b: Int
        val c: Int
        val d: Int
        if (switcher < 5) {
            a = 0
            b = 1000
            c = 1005
            d = 2000
        } else {
            a = 1005
            b = 2000
            c = 0
            d = 1000
        }
        val set: MutableSet<Int> = HashSet()
        for (i in 0 until len) {
            set.add(random(a, b))
        }
        len = set.size
        val arr = IntArray(len)
        var y = 0
        for (x in set) arr[y++] = x
        val res = IntArray(len * 2 + 2)
        System.arraycopy(arr, 0, res, 0, len)
        System.arraycopy(arr, 0, res, len, len)
        val temp = random(c, d)
        var temp2: Int
        while (true) {
            temp2 = random(c, d)
            if (temp != temp2) break
        }
        res[res.size - 2] = temp
        res[res.size - 1] = temp2
        shuffleArray(res)
        return res
    }

    private fun shuffleArray(array: IntArray) {
        var index: Int
        var temp: Int
        for (i in array.size - 1 downTo 1) {
            index = Random.nextInt(i + 1)
            temp = array[index]
            array[index] = array[i]
            array[i] = temp
        }
    }

    private fun random(l: Int, u: Int) = Random.nextInt(l, u)

    @Test
    fun basicTests() {
        assertEquals(15, SumOfArraySingles.repeats(intArrayOf(4, 5, 7, 5, 4, 8)).toLong())
        assertEquals(19, SumOfArraySingles.repeats(intArrayOf(9, 10, 19, 13, 19, 13)).toLong())
        assertEquals(12, SumOfArraySingles.repeats(intArrayOf(16, 0, 11, 4, 8, 16, 0, 11)).toLong())
    }

    @Test
    fun randomTests() {
        repeat(100) {
            val t = rand7()
            assertEquals(sumOfSingleElements(t), SumOfArraySingles.repeats(t))
        }
    }
}