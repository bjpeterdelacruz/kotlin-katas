package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestDec2Fac {
    private fun testing1(nb: Long, expect: String) {
        val actual: String = Dec2Fact.dec2FactString(nb)
        assertEquals(expect, actual)
    }

    private fun testing2(str: String, expect: Long) {
        val actual: Long = Dec2Fact.factString2Dec(str)
        assertEquals(expect, actual)
    }

    @Test
    fun basicTests1() {
        testing1(36288000L, "A0000000000")
        testing1(2982L, "4041000")
        testing1(463L, "341010")
        testing1(3628800054L, "76A0000021000")
        testing1(1273928000L, "27A0533231100")
        testing1(220L, "140200")
        testing1(1936295L, "5301133210")
        testing1(81440635L, "204365543010")
        testing1(14808485L, "40721200210")
        testing1(3395L, "4411210")
        testing1(92L, "33100")
        testing1(4881L, "6431110")
        testing1(174720L, "424400000")
        testing1(5897L, "11102210")
        testing1(1947L, "2410110")
        testing1(1575088L, "4303332200")
        testing1(49124L, "115113100")
        testing1(9376317L, "25742343110")
        testing1(449L, "332210")
        testing1(112L, "42200")
        testing1(64269L, "145123110")
        testing1(6742089L, "18515001110")
        testing1(86565208L, "218474232200")
        testing1(1806792694L, "3929024133200")
        testing1(12942219L, "35576140110")
        testing1(371993326789901217L, "311E55B5544150121110")
    }

    @Test
    fun basicTests2() {
        testing2("341010", 463L)
        testing2("4042100", 2990L)
        testing2("4041000", 2982L)
        testing2("27A0533231100", 1273928000L)
        testing2("65341010", 34303L)
        testing2("1461330110", 555555L)
        testing2("13573044440000", 7890123456L)
        testing2("1630614043233100", 1849669781372L)
        testing2("150636011110", 58322193L)
        testing2("1662032340200", 741017980L)
        testing2("194B99466413200", 145612691086L)
        testing2("51465021000", 18702054L)
        testing2("445212100", 185318L)
        testing2("1000100", 722L)
        testing2("522200", 664L)
        testing2("2000", 12L)
        testing2("64C0048313011110", 8269501168833L)
        testing2("2455221000", 916134L)
        testing2("10A739302433010", 92262000091L)
        testing2("1A20254533200", 885536614L)
        testing2("3855031110", 1440081L)
        testing2("14D4831766411000", 1739585710590L)
        testing2("331703501110", 131284689L)
        testing2("86CB45050343200", 740991913678L)
        testing2("112086032303100", 94394539820L)
        testing2("30A3700211000", 1474663950L)
        testing2("156B92750141010", 121660182223L)
        testing2("311E55B5544150121110",371993326789901217L)
    }

    private fun factZa(n: Int) = (2..n).fold(1L, Long::times)
    fun dec2FactStringZa(n: Long): String {
        var nb = n
        var q = nb / 1
        var res = "0"
        var rad: Long = 2
        while (q != 0L) {
            q = nb / rad
            val r = (nb % rad).toInt()
            res += if (r <= 9) (r + 48).toChar() else (r + 55).toChar()
            nb = q
            rad += 1
        }
        return StringBuilder(res).reverse().toString()
    }

    fun factString2DecZa(str: String): Long {
        var greatestFact = str.length - 1
        var res: Long = 0
        var i = 0
        while (i < str.length) {
            val c = str[i]
            res +=
                if (c.code <= 57) (c.code - 48) * factZa(greatestFact) else (c.code - 55) * factZa(greatestFact)
            greatestFact -= 1
            i++
        }
        return res
    }

    private fun randInt(min:Int, max:Int):Int {
        return min + (Math.random() * ((max - min) + 1)).toInt()
    }

    @Test
    fun randomTests() {
        for (i in 0..40) {
            val a: Long = randInt(12000000, 36288000).toLong()
            val sol = dec2FactStringZa(a)
            testing1(a, sol)
            val b: Long = a + randInt(500, 1000)
            val s = dec2FactStringZa(b)
            testing2(s, b)
        }
    }
}