package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.Random

class TestSimpleBeadsCount {
    @Test
    fun testFixed() {
        assertEquals(0, SimpleBeadsCount.countRedBeads(0));
        assertEquals(0, SimpleBeadsCount.countRedBeads(1));
        assertEquals(4, SimpleBeadsCount.countRedBeads(3));
        assertEquals(8, SimpleBeadsCount.countRedBeads(5));
    }

    @Test
    fun testRandom() {
        for (i in 1..100) {
            val value = Random().nextInt(5000)
            assertEquals(countRedBeadsSolution(value), SimpleBeadsCount.countRedBeads(value))
        }
    }

    // Kata author's solution
    private fun countRedBeadsSolution(nBlue: Int): Int = if (nBlue < 2) 0 else (nBlue - 1) * 2
}