package codewars

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class TestLeetspeak {
    internal var myEncoder: Leetspeak = Leetspeak()

    @Test
    fun simpleTest() {
        assertTrue(myEncoder.encode("") == "")
        assertTrue(myEncoder.encode("abcdef") == "4bcd3f")

        assertTrue(myEncoder.encode(null) == "")
        assertTrue(myEncoder.encode("xxaAeElLmMoOuUXX") == "xx443311/^^\\/^^\\00(_)(_)XX")
    }

    @Test
    fun fullTest() {
        var myEncoder: Encoder? = null
        try {
            myEncoder = Leetspeak()
        } catch (e: Exception) {
            fail("Could not initialize the encoder properly")
        }

        assertNotNull(myEncoder)
        assertTrue(myEncoder!!.encode("") == "")
        assertTrue(myEncoder.encode(null) == "")
        assertTrue(myEncoder.encode("abcd") == "4bcd")
        assertTrue(myEncoder.encode("EFGH") == "3FGH")
        assertTrue(myEncoder.encode("Leetspeak") == "133tsp34k")
        assertTrue(myEncoder.encode("3e") == "33")
        assertTrue(myEncoder.encode("109ZUI") == "109Z(_)I")
        assertTrue(myEncoder.encode("^abcD") == "^4bcD")
        assertTrue(myEncoder.encode("mVw") == "/^^\\Vw")
        assertTrue(myEncoder.encode(" ab cd ef gh ij kl mn op qr st uv wx yz\t\n\r\u2202 AB CD EF GH IJ KL MN OP QR ST UV WX YZ\t\r ") == " 4b cd 3f gh ij k1 /^^\\n 0p qr st (_)v wx yz\t\n\r\u2202 4B CD 3F GH IJ K1 /^^\\N 0P QR ST (_)V WX YZ\t\r ")
    }
}