package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class TestPlusMinusCount {
    companion object {
        fun csc(arr: Array<Int>): Int {
            var count = 0
            for (i in 1..arr.size - 1) {
                if (arr[i] >= 0 && arr[i - 1] < 0 || arr[i] < 0 && arr[i - 1] >= 0) count += 1;
            }
            return count
        }
    }
    private val random = Random()

    @Test
    fun testFixed() {
        assertEquals(1, PlusMinusCount.catchSignChange(arrayOf(-7,-7,7,0)));
        assertEquals(1, PlusMinusCount.catchSignChange(arrayOf(1,5,2,-4)));
        assertEquals(4, PlusMinusCount.catchSignChange(arrayOf(-8,4,-1,5,-3,-3,-2,-2)));
        assertEquals(1, PlusMinusCount.catchSignChange(arrayOf(-2,-2,-5,-4,5,2,0,6,0)));
        assertEquals(1, PlusMinusCount.catchSignChange(arrayOf(2,6,3,0,5,-3)));
        assertEquals(1, PlusMinusCount.catchSignChange(arrayOf(-3,3)));
        assertEquals(2, PlusMinusCount.catchSignChange(arrayOf(-1,2,2,2,2,-8,-1)));
        assertEquals(6, PlusMinusCount.catchSignChange(arrayOf(1,-2,-7,-4,4,-2,0,-3,3)));
        assertEquals(2, PlusMinusCount.catchSignChange(arrayOf(3,7,-6,2,3,1,1)));
        assertEquals(5, PlusMinusCount.catchSignChange(arrayOf(13,-7,-6,2,-1,1,-1)));
    }

    @Test
    fun testFixedZero() {
        assertEquals(0, PlusMinusCount.catchSignChange(arrayOf(4,1)));
        assertEquals(0, PlusMinusCount.catchSignChange(arrayOf<Int>()));
        assertEquals(0,PlusMinusCount. catchSignChange(arrayOf(0)));
        assertEquals(0, PlusMinusCount.catchSignChange(arrayOf(-1, -2, -3)));
    }

    @Test
    fun testRandom() {
        for (i in 1..200) {
            val arr = Array<Int>(random.nextInt(200)){_ -> random.nextInt(401) - 200}
            assertEquals(csc(arr), PlusMinusCount.catchSignChange(arr));
        }
    }
}