package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestCatalog {

    private val s = """<prod><name>drill</name><prx>99</prx><qty>5</qty></prod>

<prod><name>hammer</name><prx>10</prx><qty>50</qty></prod>

<prod><name>screwdriver</name><prx>5</prx><qty>51</qty></prod>

<prod><name>table saw</name><prx>1099.99</prx><qty>5</qty></prod>

<prod><name>saw</name><prx>9</prx><qty>10</qty></prod>

<prod><name>chair</name><prx>100</prx><qty>20</qty></prod>

<prod><name>fan</name><prx>50</prx><qty>8</qty></prod>

<prod><name>wire</name><prx>10.8</prx><qty>15</qty></prod>

<prod><name>battery</name><prx>150</prx><qty>12</qty></prod>

<prod><name>pallet</name><prx>10</prx><qty>50</qty></prod>

<prod><name>wheel</name><prx>8.80</prx><qty>32</qty></prod>

<prod><name>extractor</name><prx>105</prx><qty>17</qty></prod>

<prod><name>bumper</name><prx>150</prx><qty>3</qty></prod>

<prod><name>ladder</name><prx>112</prx><qty>12</qty></prod>

<prod><name>hoist</name><prx>13.80</prx><qty>32</qty></prod>

<prod><name>platform</name><prx>65</prx><qty>21</qty></prod>

<prod><name>car wheel</name><prx>505</prx><qty>7</qty></prod>

<prod><name>bicycle wheel</name><prx>150</prx><qty>11</qty></prod>

<prod><name>big hammer</name><prx>18</prx><qty>12</qty></prod>

<prod><name>saw for metal</name><prx>13.80</prx><qty>32</qty></prod>

<prod><name>wood pallet</name><prx>65</prx><qty>21</qty></prod>

<prod><name>circular fan</name><prx>80</prx><qty>8</qty></prod>

<prod><name>exhaust fan</name><prx>62</prx><qty>8</qty></prod>

<prod><name>window fan</name><prx>62</prx><qty>8</qty></prod>"""

    // Kata author's solution
    private fun _catalog(s: String, article: String): String {
        val r = mutableListOf<String>()
        s.split(Regex("""\n+""")).forEach{
            val line = it.slice(12..it.length-14).split(Regex("""<.*?><.*?>"""))
            if (line[0].contains(article)) {
                r.add("${line[0]} > prx: $${line[1]} qty: ${line[2]}")
            }
        }
        return if (r.size > 0) r.joinToString("\n") else "Nothing"
    }
    private fun rand(from: Int, to: Int) : Int {
        return Math.floor((to - from + 1) * Math.random()+from).toInt()
    }

    private val ctgr = arrayOf("saw", "table saw", "saw for wood", "hammer", "big hammer", "fan", "exhaust fan", "circular fan", "window fan", "wheel", "bicycle wheel", "ladder", "battery", "car battery", "bumper", "platform", "hoist", "wood pallet", "extractor", "screwdriver") // 20
    private val prices = "120.3 13.1 17.6 93.5 3.2 71.4 12.2 120.90 34.00 13.6 11.00 12.00 110.7 24.8 54.00 17.5 128.00 17.00 19.00 20.00".split(" ") // 20
    private val qty = arrayOf(50, 20, 12, 8, 6, 45, 32, 12, 51, 2, 26, 17, 33, 14, 15, 16, 7, 8, 9, 20)
    private val fks = arrayOf("scissors", "nails", "knives", "rings", "stars", "roofs", "trucks", "doors", "lighters")

    private fun compose321(k: Int): String {
        val res = mutableListOf<String>()
        for (i in 1..k) {
            val n = ctgr[rand(0, ctgr.size - 1)]
            val a = prices[rand(0, prices.size - 1)]
            val y = qty[rand(0, qty.size - 1)]
            res.add("<prod><name>${n}</name><prx>${a}</prx><qty>${y}</qty></prod>")
        }
        return res.joinToString("\n\n")
    }

    @Test
    fun `Basic tests`() {
        assertEquals("ladder > prx: $112 qty: 12", Catalog.catalog(s, "ladder"))
        assertEquals("table saw > prx: $1099.99 qty: 5\nsaw > prx: $9 qty: 10\nsaw for metal > prx: $13.80 qty: 32", Catalog.catalog(s, "saw"))
        assertEquals("wood pallet > prx: $65 qty: 21", Catalog.catalog(s, "wood pallet"))
        assertEquals("extractor > prx: $105 qty: 17", Catalog.catalog(s, "extractor"))
        assertEquals("Nothing", Catalog.catalog(s, "nails"))
        assertEquals("battery > prx: $150 qty: 12", Catalog.catalog(s, "battery"))
        assertEquals("wheel > prx: $8.80 qty: 32\ncar wheel > prx: $505 qty: 7\nbicycle wheel > prx: $150 qty: 11", Catalog.catalog(s, "wheel"))
        assertEquals("table saw > prx: $1099.99 qty: 5", Catalog.catalog(s, "table saw"))

        assertEquals("exhaust fan > prx: $62 qty: 8", Catalog.catalog(s, "exhaust fan"))
        assertEquals("platform > prx: $65 qty: 21", Catalog.catalog(s, "platform"))
        assertEquals("fan > prx: $50 qty: 8\ncircular fan > prx: $80 qty: 8\nexhaust fan > prx: $62 qty: 8\nwindow fan > prx: $62 qty: 8", Catalog.catalog(s, "fan"))
        assertEquals("hoist > prx: $13.80 qty: 32", Catalog.catalog(s, "hoist"))
        assertEquals("big hammer > prx: $18 qty: 12", Catalog.catalog(s, "big hammer"))
        assertEquals("window fan > prx: $62 qty: 8", Catalog.catalog(s, "window fan"))

        assertEquals("screwdriver > prx: $5 qty: 51", Catalog.catalog(s, "screwdriver"))
        assertEquals("hammer > prx: $10 qty: 50\nbig hammer > prx: $18 qty: 12", Catalog.catalog(s, "hammer"))
        assertEquals("Nothing", Catalog.catalog(s, "scissors"))
        assertEquals("bicycle wheel > prx: $150 qty: 11", Catalog.catalog(s, "bicycle wheel"))
    }

    @Test
    fun `100 Random tests`() {
        for (i in 1..100) {
            val   rn = rand(0, 21)
            val f = if (0 == rn % 7) fks[rand(0, fks.size - 1)] else ctgr[rand(0, ctgr.size - 1)]
            val s = compose321(rand(5, 10))
            assertEquals(_catalog(s, f), Catalog.catalog(s, f))
        }
    }
}