package codewars

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TestInt132 {
    private fun doIntTest(a: Int) {
        val b = Int132.int123(a);
        Assertions.assertTrue(b >= 0);
        Assertions.assertEquals(123, (a + b).toInt());
    }

    @Test
    fun testLess() {
        doIntTest(0);
    }

    @Test
    fun testSame() {
        doIntTest(123);
    }

    @Test
    fun testGreater() {
        doIntTest(500);
    }

    @Test
    fun testMax() {
        doIntTest(Integer.MAX_VALUE);
    }

    @Test
    fun testMin() {
        doIntTest(Integer.MIN_VALUE);
    }

    @Test
    fun testRandom() {
        val lo = -10_000_000
        val hi = 10_000_000
        for (r in 1..10000) {
            val a = (Math.random() * (hi - lo)).toInt() + lo;
            doIntTest(a);
        }
    }

}