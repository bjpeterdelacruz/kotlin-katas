package codewars

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.random.Random

class TestAlternateCapitalization {
    @Test
    fun basicTests() {
        assertEquals(listOf("AbCdEf", "aBcDeF"), AlternateCapitalization.capitalize("abcdef"))
        assertEquals(listOf("CoDeWaRs", "cOdEwArS"), AlternateCapitalization.capitalize("codewars"))
        assertEquals(listOf("AbRaCaDaBrA", "aBrAcAdAbRa"), AlternateCapitalization.capitalize("abracadabra"))
        assertEquals(listOf("CoDeWaRrIoRs", "cOdEwArRiOrS"), AlternateCapitalization.capitalize("codewarriors"))
        assertEquals(listOf("InDeXiNgLeSsOnS", "iNdExInGlEsSoNs"), AlternateCapitalization.capitalize("indexinglessons"))
        assertEquals(listOf("CoDiNgIsAfUnAcTiViTy", "cOdInGiSaFuNaCtIvItY"), AlternateCapitalization.capitalize("codingisafunactivity"))
    }

    private val letters = "abcdefghijklmnopqrstuvwxyz"

    @Test
    fun randomTests() {
        repeat(100) {
            val count = Random.nextInt(10, 20)

            val randomString = letters.split("").shuffled().take(count).joinToString("")

            assertEquals(internalCapitalize(randomString), AlternateCapitalization.capitalize(randomString))
        }
    }

    // Kata author's solution
    private fun internalCapitalize(text: String): List<String> =
        listOf(
            { ind: Int -> ind % 2 == 0 },
            { ind: Int -> ind % 2 != 0 }
        ).map { func ->
            text
                .mapIndexed { index, c -> if (func(index)) c.uppercaseChar() else c }
                .joinToString("")
        }
}