package codewars

import AlphabetWar
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestAlphabetWars {
    @Test
    fun testFixed() {
        assertEquals("Right side wins!", AlphabetWar.alphabetWar("z"))
        assertEquals("Let's fight again!",AlphabetWar.alphabetWar("zdqmwpbs"))
        assertEquals("Right side wins!", AlphabetWar.alphabetWar("zzzzs"))
        assertEquals("Left side wins!", AlphabetWar.alphabetWar("wwwwww"))
    }
}