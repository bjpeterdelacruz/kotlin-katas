package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TestFindIntersection {
    private val challenge = FindIntersection()

    @Test
    fun findIntersection1() {
        Assertions.assertEquals("false", challenge.findIntersection(arrayOf("1, 3, 5, 7", "2, 4, 6, 8")))
    }

    @Test
    fun findIntersection2() {
        Assertions.assertEquals("4,5,19", challenge.findIntersection(arrayOf("4, 5, 7, 11, 19, 20", "4, 5, 19")))
    }

    @Test
    fun findIntersection3() {
        Assertions.assertEquals("12", challenge.findIntersection(arrayOf("12, 24, 36, 41, 45", "12, 13")))
    }

    @Test
    fun findIntersection4() {
        Assertions.assertEquals("1,7", challenge.findIntersection(arrayOf("1, 3, 5, 7", "1, 2, 6, 7")))
    }
}