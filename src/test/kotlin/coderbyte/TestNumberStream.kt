package coderbyte

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class TestNumberStream {
    private val challenge = NumberStream()

    @Test
    fun numberStream1() {
        Assertions.assertTrue(challenge.numberStream("6539923335"))
    }

    @Test
    fun numberStream2() {
        Assertions.assertFalse(challenge.numberStream("5556293383563665"))
    }

    @Test
    fun numberStream3() {
        Assertions.assertTrue(challenge.numberStream("5788888888882339999"))
    }
}