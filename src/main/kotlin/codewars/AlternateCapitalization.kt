package codewars

object AlternateCapitalization {
    fun capitalize(text: String): List<String> {
        val even = ArrayList<Char>()
        val odd = ArrayList<Char>()
        text.forEachIndexed { index, character ->
            if (index % 2 == 0) {
                even.add(character.uppercaseChar())
                odd.add(character)
            }
            else {
                even.add(character)
                odd.add(character.uppercaseChar())
            }
        }
        return listOf(even.joinToString(""), odd.joinToString(""))
    }
}