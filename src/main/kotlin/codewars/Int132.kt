package codewars

object Int132 {
    fun int123(a: Int): Long {
        if (-123 <= a && a <= 123) {
            return 123L - a
        }
        if (a > 123) {
            return Long.MAX_VALUE - (a - 123) + 1L
        }
        if (a == Integer.MIN_VALUE) {
            return Integer.MAX_VALUE + 124L
        }
        return -a + (Long.MAX_VALUE * Long.MAX_VALUE) + 122
    }
}