package codewars

object SumOfArraySingles {
    fun repeats(arr: IntArray): Int {
        val counts = HashMap<Int, Int>()
        arr.forEach {
            counts[it] = counts.getOrDefault(it, 0) + 1
        }
        return counts.filter {
            it.value == 1
        }.map { it.key }.toList().sum()
    }
}