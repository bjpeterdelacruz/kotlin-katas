package codewars

object PlusMinusCount {
    fun catchSignChange(arr: Array<Int>): Int {
        val strings = arr.map { it.toString() }.toList()
        var count = 0
        for (idx in 1 until strings.size) {
            if (strings[idx - 1].indexOf("-") != strings[idx].indexOf("-")) {
                count++
            }
        }
        return count
    }
}