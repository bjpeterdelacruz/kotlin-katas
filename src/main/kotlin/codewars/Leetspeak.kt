package codewars

class Leetspeak : Encoder() {
    override fun encode(source: String?): String {
        if (source == null) {
            return ""
        }
        val leetspeak = mapOf("a" to 4, "A" to 4, "e" to 3, "E" to 3, "l" to 1, "L" to 1,
            "m" to "/^^\\", "M" to "/^^\\", "o" to 0, "O" to 0, "u" to "(_)", "U" to "(_)")
        return source.map { leetspeak.getOrDefault(it.toString(), it.toString()) }.joinToString("")
    }
}

abstract class Encoder {
    abstract fun encode(source: String?): String
}