package codewars

object BabyCounting {
    fun babyCount(x: String): Int? {
        val counts = HashMap<Char, Int>()
        x.lowercase().forEach { counts[it] = counts.getOrDefault(it, 0) + 1 }
        var count = 0
        while (counts.getOrDefault('b', 0) >= 2
            && counts.getOrDefault('a', 0) >= 1
            && counts.getOrDefault('y', 0) >= 1) {
            counts['b'] = counts.getOrDefault('b', 0) - 2
            counts['a'] = counts.getOrDefault('a', 0) - 1
            counts['y'] = counts.getOrDefault('y', 0) - 1
            count++
        }
        return if (count > 0) { count } else { null }
    }
}