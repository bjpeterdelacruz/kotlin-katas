package codewars

import kotlin.collections.ArrayList

object Catalog {
    fun catalog(s: String, article: String): String {
        val catalogItems = ArrayList<String>()
        s.split("\n").forEach { line ->
            if (!line.isEmpty()) {
                val name = line.substring(line.indexOf("<name>") + 6, line.indexOf("</name>"))
                val price = "$" + line.substring(line.indexOf("<prx>") + 5, line.indexOf("</prx>"))
                val quantity = line.substring(line.indexOf("<qty>") + 5, line.indexOf("</qty>"))
                catalogItems.add("$name > prx: $price qty: $quantity")
            }
        }
        val foundItems = catalogItems.filter { item -> item.contains(article) }.toList()
        return if (foundItems.isEmpty()) "Nothing" else foundItems.joinToString(separator = "\n")
    }
}