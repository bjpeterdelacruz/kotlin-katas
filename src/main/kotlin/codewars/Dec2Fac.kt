package codewars

object Dec2Fact {
    private fun alphaNum(): Map<String, Long> {
        return mapOf("A" to 10L, "B" to 11L, "C" to 12L, "D" to 13L, "E" to 14L, "F" to 15L,
            "G" to 16L, "H" to 17L, "I" to 18L, "J" to 19L, "K" to 20L, "L" to 21L, "M" to 22L, "N" to 23L, "O" to 24L,
            "P" to 25L, "Q" to 26L, "R" to 27L, "S" to 28L, "T" to 29L, "U" to 30L, "V" to 31L, "W" to 32L, "X" to 33L,
            "Y" to 34L, "Z" to 35L)
    }

    private fun numAlpha(): Map<Long, String> {
        return alphaNum().entries.associateBy({ it.value }) { it.key }
    }

    fun dec2FactString(n: Long): String {
        var result = ""
        var count = 1
        var dividend = n
        val numAlpha = numAlpha()
        do {
            val remainder = dividend % count
            val quotient = dividend / count
            var temp = remainder.toString()
            if (remainder > 9) {
                temp = numAlpha[remainder].toString()
            }
            result = temp + result
            dividend = quotient
            count++
        } while (quotient != 0L)
        return result
    }

    fun factString2Dec(str: String): Long {
        var length = str.length - 1
        var idx = 0
        var sum = 0L
        val alphaNum = alphaNum()
        do {
            val num = alphaNum[str[idx].toString()]?: Character.getNumericValue(str[idx]).toLong()
            sum += num * (1L..length).reduce(Long::times)
            idx++
            length--
        } while (length != 0)
        return sum
    }
}