package codewars

object SimpleBeadsCount {
    fun countRedBeads(nBlue: Int): Int = if (nBlue < 2) 0 else (nBlue - 1) * 2
}