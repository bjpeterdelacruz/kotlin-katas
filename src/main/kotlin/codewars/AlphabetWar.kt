object AlphabetWar {
    fun alphabetWar(fight: String): String {
        val left = mapOf('w' to 4, 'p' to 3, 'b' to 2, 's' to 1)
        val right = mapOf('m' to 4, 'q' to 3, 'd' to 2, 'z' to 1)
        var totalLeft = 0
        var totalRight = 0

        for (s in fight.chars()) {
            totalLeft += left.getOrDefault(s.toChar(), 0)
            totalRight += right.getOrDefault(s.toChar(), 0)
        }

        if (totalLeft > totalRight) {
            return "Left side wins!"
        } else if (totalRight > totalLeft) {
            return "Right side wins!"
        }
        return "Let's fight again!"
    }
}